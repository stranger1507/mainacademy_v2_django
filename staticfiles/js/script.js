$('[data-action="like"]').on('click', function() {
  var $btn = $(this);
  $.get($btn.data('url'), function( e ) {
    if (e.status === 'ok')
    {
      if (e.counter > 0)
        $btn.find('span#counter').html(e.counter);
      else
        $btn.find('span#counter').html('');
    } else{
      alert(e)
    }
  });
});
