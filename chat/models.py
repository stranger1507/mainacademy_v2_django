from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Room(models.Model):
    name = models.CharField(_('Room name'), max_length=255)
    created_datetime = models.DateTimeField(_('Created datetime'), auto_now_add=True)
    updated_datetime = models.DateTimeField(_('Updated datetime'), auto_now=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = _('Комната')
        verbose_name_plural = _('Комнаты')


class Message(models.Model):
    created_datetime = models.DateTimeField(_('Created datetime'), auto_now_add=True)
    updated_datetime = models.DateTimeField(_('Updated datetime'), auto_now=True)
    message = models.TextField(_('Message'), max_length=1000)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    room = models.ForeignKey('chat.Room', on_delete=models.CASCADE)

    def __str__(self):
        return "{0} {1}".format(self.author, self.room)

    class Meta:
        verbose_name = _('Сообщение')
        verbose_name_plural = _('Сообщения')
