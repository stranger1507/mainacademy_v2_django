from django.contrib import admin

from .models import Room, Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('author', 'room', 'created_datetime',)


admin.site.register(Room)
admin.site.register(Message, MessageAdmin)
