import json

from channels.channel import Group
from django.template.loader import render_to_string

from chat.models import Message


def get_room_id(message):
    return message.content.get('path').replace('/', '')


def ws_connect(message):
    room_id = get_room_id(message)
    message.reply_channel.send({"accept": True})
    Group(room_id).add(message.reply_channel)


def ws_message(message):
    room_id = get_room_id(message)
    data = json.loads(message.content['text'])
    msg_obj = Message.objects.create(
        message=data['message'], author_id=data['author_id'], room_id=room_id
    )
    Group(room_id).send({
        'text': json.dumps({
            'message': msg_obj.message, 'author': str(msg_obj.author),
            'message_html': render_to_string('chat/includes/chat_message.html', {'msg': msg_obj})
        })
    })


def ws_disconnect(message):
    room_id = get_room_id(message)
    Group(room_id).discard(message.reply_channel)
