from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView

from .models import Room


class RoomListView(LoginRequiredMixin, ListView):
    model = Room
    template_name = 'chat/chat_list.html'


class RoomDetailView(LoginRequiredMixin, DetailView):
    model = Room
    template_name = 'chat/detail.html'
