from django.urls import path

from chat.views import RoomListView, RoomDetailView

app_name = 'Чат'

urlpatterns = [
    path(r'list/', RoomListView.as_view(), name='room-list'),
    path(r'detail/<int:pk>/', RoomDetailView.as_view(), name='room-detail'),
]
