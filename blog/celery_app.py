import os
from datetime import timedelta

from celery import Celery
from celery.task import periodic_task

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'blog.setting.base')

app = Celery()

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


@app.task
def debug_task():
    print('Request:')


@app.task
def debug_task_one():
    for x in range(1000):
        a = x ** x


@app.task
def debug_task_two():
    for x in range(1000000000000000000000):
        a = x ** x
        if x > 1000:
            raise ValueError('It is error text')


@periodic_task(run_every=timedelta(days=1))
def super_periodic_task():
    pass
