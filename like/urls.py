from django.urls import path

from .views import *
app_name = 'like'

urlpatterns = [
    path(r'create/<int:type>/<int:id>/', LikeCreateView.as_view(), name='like'),
]
