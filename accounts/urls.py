from django.urls import path

from accounts.views import CustomLoginView, CustomLogoutView, RegisterCreateView, UserUpdateView
app_name = 'accounts'

urlpatterns = [
    path(r'login/', CustomLoginView.as_view(), name='login'),
    path(r'logout/', CustomLogoutView.as_view(), name='logout'),
    path(r'registration/', RegisterCreateView.as_view(), name='registration'),
    path(r'update/<int:pk>/', UserUpdateView.as_view(), name='update'),
]
