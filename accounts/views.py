from braces.views import AnonymousRequiredMixin
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, UpdateView

from accounts.forms import LoginForm, CustomUserCreationForm, CustomUserUpdateForm


class CustomLoginView(SuccessMessageMixin, AnonymousRequiredMixin, LoginView):
    template_name = 'accounts/login.html'
    success_url = '/'
    form_class = LoginForm
    success_message = _('Вы удачно авторизовались!')


class CustomLogoutView(LogoutView):
    next_page = '/'


class RegisterCreateView(AnonymousRequiredMixin, CreateView):
    model = get_user_model()
    form_class = CustomUserCreationForm
    template_name = 'accounts/login.html'

    def get_success_url(self):
        return reverse_lazy('accounts:login')


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = get_user_model()
    form_class = CustomUserUpdateForm
    template_name = 'accounts/login.html'
    success_url = '/'

    def get_queryset(self):
        return self.model.objects.filter(id=self.request.user.id)

    def form_invalid(self, form):
        print(form.errors)
        return super(UserUpdateView, self).form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('accounts:update', kwargs={'pk': self.request.user.id})

    def dispatch(self, request, *args, **kwargs):
        if kwargs['pk'] == request.user.id:
            return super(UserUpdateView, self).dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(self.get_success_url())
