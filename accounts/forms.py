from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext_lazy as _


class LoginForm(AuthenticationForm):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(
            attrs={
                'autofocus': True,
                'class': 'form-control'
            }
        ),
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )


class CustomUserCreationForm(UserCreationForm):
    password1 = forms.CharField(
        label=_("Пароль"),
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )
    password2 = forms.CharField(
        label=_("Пароль 2"),
        strip=False,
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )
    first_name = forms.CharField(
        label=_("Имя"),
        strip=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )
    last_name = forms.CharField(
        label=_("Фамилия"),
        strip=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )
    email = forms.CharField(
        label=_("Email"),
        strip=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name')


class CustomUserUpdateForm(forms.ModelForm):
    first_name = forms.CharField(
        label=_("Имя"),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )
    last_name = forms.CharField(
        label=_("Фамилия"),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control'
            }
        ),
    )
    avatar = forms.ImageField(
        label=_("Фото"),
        widget=forms.FileInput(
            attrs={
                'class': 'form-control'
            },
        ),
        required=False
    )

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'avatar',)
