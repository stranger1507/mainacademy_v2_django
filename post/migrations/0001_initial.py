# Generated by Django 2.0.5 on 2018-11-12 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.PositiveIntegerField(primary_key=True, serialize=False, unique=True, verbose_name='ID пользователя')),
                ('created_datetime', models.DateTimeField(auto_now_add=True, verbose_name='Время создание')),
                ('update_datetime', models.DateTimeField(auto_now=True, verbose_name='Время последнего изменения')),
                ('title', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('text', models.TextField(verbose_name='Текст новости')),
            ],
        ),
    ]
