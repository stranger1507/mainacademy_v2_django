import django_filters

from .models import Post


class PostFilters(django_filters.FilterSet):
    filter_title = django_filters.CharFilter(field_name='title', lookup_expr='icontains')
    text = django_filters.CharFilter(lookup_expr='icontains')
    comments_count = django_filters.NumberFilter(label='comments_count', field_name='comments_count')
    likes_count = django_filters.NumberFilter(label='likes_count', field_name='likes_count')
    order_by = django_filters.OrderingFilter(
        fields=['title', 'created_datetime', 'author__email', 'comments_count', 'likes_count']
    )

    class Meta:
        model = Post
        fields = ('filter_title', 'text', 'author', 'comments_count', 'likes_count')
