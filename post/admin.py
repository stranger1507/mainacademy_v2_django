from django.contrib import admin

from .models import Post, Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_datetime', 'updated_datetime', 'id']
    list_filter = ['created_datetime', 'updated_datetime']
    search_fields = ['title', 'text', 'id']


@admin.register(Comment)
class PostAdmin(admin.ModelAdmin):
    list_display = ['author', 'created_datetime', 'updated_datetime', 'id']
    list_filter = ['created_datetime', 'updated_datetime']
    search_fields = ['text', 'id']
