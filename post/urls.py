"""untitled3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views

app_name = 'posts'

urlpatterns = [
    path(r'', views.PostListView.as_view(), name='index'),
    path(r'edit/comment/<int:pk>/', views.CommentUpdateView.as_view(), name='comment-edit'),
    path(r'detail/comment/<int:pk>/', views.CommentDetailView.as_view(), name='comment-detail'),
    path(r'create_comment/<int:post_id>/', views.CommentCreateView.as_view(), name='create-comment'),
    path(r'detail/<int:pk>/', views.PostDetailView.as_view(), name='detail'),
    path(r'edit/post/<int:pk>/', views.PostUpdateView.as_view(), name='edit'),
    path(r'delete/<int:pk>/', views.PostDeleteView.as_view(), name='delete'),
    path(r'create/', views.PostCreateView.as_view(), name='create'),
]
