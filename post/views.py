from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, CreateView, DeleteView, UpdateView
from django_filters.views import FilterView

from post.filters import PostFilters
from .forms import CreatePostForm, CommentForm
from .mixins import UserPostMixin
from .models import Post, Comment


def index(request):
    context = {'object_list': Post.objects.all()}
    return render(request, 'post/list.html', context)


def detail(request, *args, **kwargs):
    """
    отобразить все объекты из модели objects.all()
    найти только один объект из бд objects.get(param=data)
    найти все объекты  из бд подходящие по параметрам objects.filter(param=data)
    """
    context = {'object': get_object_or_404(Post, id=kwargs.get('pk'))}
    return render(request, 'post/detail.html', context)


def delete(request, *args, **kwargs):
    """
    GET/POST
    """
    if request.method == 'POST':
        post = get_object_or_404(Post, id=kwargs.get('pk'))
        post.delete()
        return HttpResponseRedirect('/')
    else:
        return render(request, 'post/delete_confirmation.html', {})


def create(request, *args, **kwargs):
    form = CreatePostForm
    if request.method == 'GET':
        return render(request, 'post/form.html', {'form': form})
    else:
        new_form = form(data=request.POST)
        if new_form.is_valid():
            new_form.save()
            return HttpResponseRedirect('/')
        else:
            return render(request, 'post/form.html',
                          {'form': form, 'btn_text': _('Создать объявление')})


def edit(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'post/form.html', {'form': CreatePostForm(instance=post),
                                                  'btn_text': _('Редактировать объявление')})
    else:
        form = CreatePostForm(instance=post, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('post:detail', kwargs={'pk': post.pk}))
        else:
            return render(request, 'post/form.html',
                          {'form': form, 'btn_text': _('Редактировать объявление')})


class PostListView(FilterView):
    queryset = Post.objects.all().annotate(likes_count=Count('likes'))
    template_name = 'post/list.html'
    paginate_by = 2
    filterset_class = PostFilters


class PostDetailView(DetailView):
    template_name = 'post/detail.html'
    queryset = Post.objects.all()

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context.update({'form': CommentForm})
        return context


class PostCreateView(LoginRequiredMixin, CreateView):
    form_class = CreatePostForm
    template_name = 'post/form.html'
    queryset = Post.objects.all()

    def form_valid(self, form):
        new_form = form.save(commit=False)
        new_form.author = self.request.user
        new_form.save()
        return HttpResponseRedirect('/')


class PostDeleteView(UserPostMixin, DeleteView):
    template_name = 'post/delete_confirmation.html'
    success_url = '/'


class PostUpdateView(UserPostMixin, UpdateView):
    success_url = '/'
    form_class = CreatePostForm
    template_name = 'post/form.html'

    def get_context_data(self, **kwargs):
        context = super(PostUpdateView, self).get_context_data(**kwargs)
        context.update({'btn_text': _('Редактировать объявление')})
        return context

    def get_success_url(self):
        return reverse_lazy('post:detail', kwargs={'pk': self.object.pk})


class CommentCreateView(LoginRequiredMixin, CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'post/detail.html'
    success_url = '/'

    def form_valid(self, form):
        data_form = form.save(commit=False)
        data_form.author = self.request.user
        data_form.post_id = self.kwargs['post_id']
        data_form.save()
        return JsonResponse({
            'status': 'ok',
            'comment': render_to_string('post/includes/comment.html',
                                        context={'comment': data_form})
        })

    def form_invalid(self, form):
        return JsonResponse({'status': 'fail', 'form': str(form)})


class CommentDetailView(LoginRequiredMixin, UpdateView):
    model = Comment
    template_name = 'post/includes/comment.html'
    context_object_name = 'comment'
    fields = '__all__'


class CommentUpdateView(LoginRequiredMixin, UpdateView):
    model = Comment
    form_class = CommentForm
    template_name = 'post/comment_form.html'
    success_url = '/'

    def form_valid(self, form):
        super(CommentUpdateView, self).form_valid(form)
        return JsonResponse({
            'status': 'ok',
            'text': render_to_string(
                'post/includes/comment.html',
                context={'comment': self.object, 'user': self.request.user}
            )
        })

    def form_invalid(self, form):
        super(CommentUpdateView, self).form_invalid(form)
        return JsonResponse({
            'status': 'error',
            'form': render_to_string(
                self.template_name,
                context=self.get_context_data(),
                request=self.request
            )
        })
