from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import ugettext_lazy as _


class DatesAbstractModel(models.Model):
    created_datetime = models.DateTimeField(_('Время создание'), auto_now_add=True)
    updated_datetime = models.DateTimeField(_('Время последнего изменения'), auto_now=True)
    author = models.ForeignKey('accounts.User', on_delete=models.CASCADE, verbose_name=_('Автор'))

    class Meta:
        abstract = True


class Post(DatesAbstractModel):
    title = models.CharField(_('Заголовок'), max_length=255)
    text = models.TextField(_('Текст новости'))
    likes = GenericRelation('like.Like', related_name='post_likes')

    def __str__(self):
        return " ".join([str(self.created_datetime), self.title])

    class Meta:
        ordering = ('-created_datetime',)
        verbose_name = _('Новость')
        verbose_name_plural = _('Новости')


class Comment(DatesAbstractModel):
    text = models.TextField(_('Комментарии'))
    post = models.ForeignKey('post.Post', on_delete=models.CASCADE, verbose_name=_('Новость'))
    likes = GenericRelation('like.Like', related_name='comment_likes')

    def __str__(self):
        return "Комментарий к новости - {0}".format(self.id)

    class Meta:
        ordering = ('created_datetime',)
        verbose_name = _('Комментарий')
        verbose_name_plural = _('Комментарии')
