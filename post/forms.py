from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Post, Comment


class CreatePostForm(forms.ModelForm):
    # title = forms.CharField(widget=forms.Textarea(attrs={'class': 'success'}))
    # text = forms.CharField(widget=forms.TextInput(attrs={'class': 'text'}))

    # def clean_text(self):
    #     raise forms.ValidationError('error in text')
    # def clean_title(self):
    #     raise forms.ValidationError('error in title')
    #
    # def clean(self):
    #     cleaned_data = super(CreatePostForm, self).clean()
    #     if cleaned_data.get('text'):
    #         self.add_error('text', 'error')
    #     if cleaned_data.get('title'):
    #         self.add_error('title', 'error title')
    #     return cleaned_data

    class Meta:
        model = Post
        fields = ['title', 'text']
        widgets = {
            'title': forms.TextInput(),
            'text': forms.Textarea(),
        }


class CommentForm(forms.ModelForm):
    text = forms.CharField(
        label='',
        widget=forms.Textarea(attrs={'placeholder': _('Введите ваш комментарий')})
    )

    class Meta:
        model = Comment
        fields = ['text']
